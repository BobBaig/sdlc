package com.baigsoft.jms.listener;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

/* The commented-out imports and logger code were in the SDLC tutorial, but not
 *  printing the time stamp. Using alternative imports and different logger
 *  definition suggested in stackoverflow fixed it. Checked log file at:
 *  C:\000\apache-tomcat-7.0.82\logs\consumer.log => it is also being created.
 */
//import org.apache.log4j.LogManager;
//import org.apache.log4j.Logger;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.baigsoft.jms.adapter.ConsumerAdapter;

/** -MDB- Message-Driven JavaBean.  Implements Message Listener */
@Component
public class ConsumerListener implements MessageListener {
//	private static Logger logger = LogManager.getLogger(ConsumerListener.class.getName());
	private static final Logger logger = LogManager.getLogger();

	@Autowired
	JmsTemplate jmsTemplate;
	@Autowired
	ConsumerAdapter consumerAdapter;

	@Override
	public void onMessage(Message message) {
		String json = null;

//		System.out.println("HELLO In onMessage()");
		logger.info("In onMessage()");
		if (message instanceof TextMessage) {
			try {
				json = ((TextMessage)message).getText();
				logger.info("Sending JSON to db: " + json);
				consumerAdapter.sendToMongo(json);
			} catch (Exception e) {
				logger.error("Message: " + json);
				jmsTemplate.convertAndSend(json);
			}
		} else {
			logger.info("Message type isn't TextMessage");
		}
	}
}